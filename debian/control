Source: tpot
Section: python
Priority: optional
Maintainer: Debian Science Maintainers <debian-science-maintainers@lists.alioth.debian.org>
Uploaders:
    Christian Kastner <ckk@debian.org>,
Build-Depends:
    debhelper-compat (= 13),
    dh-python,
    mkdocs,
    help2man,
    python3-all,
    python3-deap (>= 1.2),
    python3-joblib (>= 0.13.2),
    python3-imblearn (>= 0.7.0),
    python3-nose (>= 1.3.7) <!nocheck>,
    python3-numpy (>= 1.16.3),
    python3-pandas (>= 0.24.2),
    python3-scipy (>= 1.3.1),
    python3-setuptools,
    python3-sklearn (>= 1.0),
    python3-stopit (>= 1.1.1),
    python3-torch,
    python3-tqdm (>= 4.36.1),
    python3-xgboost (>= 1.1.0),
Rules-Requires-Root: no
Standards-Version: 4.6.0
Homepage: https://epistasislab.github.io/tpot/
Vcs-Git: https://salsa.debian.org/science-team/tpot.git
Vcs-Browser: https://salsa.debian.org/science-team/tpot

Package: python3-tpot
Architecture: all
Depends:
    ${misc:Depends},
    ${python3:Depends},
Recommends:
    python3-dask,
    python3-distributed,
    python3-torch,
Description: Automated Machine Learning tool built on top of scikit-learn
 Consider TPOT your Data Science Assistant. TPOT is a Python Automated Machine
 Learning tool that optimizes machine learning pipelines using genetic
 programming.
 .
 TPOT will automate the most tedious part of machine learning by intelligently
 exploring thousands of possible pipelines to find the best one for your data.
 .
 Once TPOT is finished searching (or you get tired of waiting), it provides you
 with the Python code for the best pipeline it found so you can tinker with the
 pipeline from there.
 .
 TPOT is built on top of scikit-learn, so all of the code it generates should
 look familiar... if you're familiar with scikit-learn, anyway.
 .
 This package contains the Python 3.x version of TPOT.

Package: python-tpot-doc
Architecture: all
Section: doc
Build-Profiles: <!nodoc>
Depends:
    ${misc:Depends},
    ${mkdocs:Depends},
Description: documentation and examples for TPOT
 Consider TPOT your Data Science Assistant. TPOT is a Python Automated Machine
 Learning tool that optimizes machine learning pipelines using genetic
 programming.
 .
 TPOT will automate the most tedious part of machine learning by intelligently
 exploring thousands of possible pipelines to find the best one for your data.
 .
 Once TPOT is finished searching (or you get tired of waiting), it provides you
 with the Python code for the best pipeline it found so you can tinker with the
 pipeline from there.
 .
 TPOT is built on top of scikit-learn, so all of the code it generates should
 look familiar... if you're familiar with scikit-learn, anyway.
 .
 This package contains the documentation, example scripts, and tutorials for
 TPOT.
